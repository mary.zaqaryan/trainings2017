#include<iostream>

int
main()
{
    int x = 9, y = 3, a = 6, b = 5, g = 5, i = 1, j = 12;
    if ((!(x < 5) && !(y >= 7)) == (!(x < 5) || (y >= 7))) {
        std::cout << "This expressions are equal" << std::endl;
    }
    if ((!(a == b) || !(g != 5)) == (!((a == b) && (g != 5)))) {
        std::cout << "This expressions are equal" << std::endl;
    }
    if ((!((x <= 8) && (y > 4))) == (!((x <= 8) || (y > 4)))) {
        std::cout  << "This expressions are equal" <<  std::endl;
    }
    if ((!(i > 4) || (j <= 6)) == (!(i > 4) && !(j <= 6))) {
        std::cout << "This expressions are equal" << std::endl;        
    }
    return 0;
}  
