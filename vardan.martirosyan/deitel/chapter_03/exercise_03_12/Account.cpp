#include "Account.hpp"
#include <iostream>

Account::Account(int accountBalance)
{
    setBalance(accountBalance);
} 

void
Account::setBalance(int balance)
{
    if (balance < 0) {
        balance = 0;
        std::cout << "Info 1: Your balance is negative. It is automatically setted to 0. " << std::endl;
        return;
    }
    accountBalance_ = balance;
}

int
Account::getBalance(int amount)
{
    return accountBalance_;
}

int 
Account::credit(int moneyAdd)
{
    accountBalance_ += moneyAdd;
    return accountBalance_;
}

int 
Account::debit(int moneyWithdraw)
{
    if (moneyWithdraw > accountBalance_) {
        std::cout << "Info 2: Debit amount exceeded account balance." << std::endl;
        return accountBalance_;
    }
    accountBalance_ -= moneyWithdraw;
    return accountBalance_;
}
