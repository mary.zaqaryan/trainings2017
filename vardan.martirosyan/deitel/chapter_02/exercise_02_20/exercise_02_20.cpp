#include <iostream>

int
main()
{
    int r;

    std::cout << "Enter R: ";
    std::cin  >> r;

    if (r < 0) {
        std::cout << "The radius can not be negative. " << std::endl;
        return 1;
    }
    std::cout << "Diameter = " << (2 * r)     << std::endl;
    std::cout << "Square = "   << (3.14159 * r * r) << std::endl;
    std::cout << "Lenght = "   << (3.14159 * r * 2) << std::endl;

    return 0;
}

