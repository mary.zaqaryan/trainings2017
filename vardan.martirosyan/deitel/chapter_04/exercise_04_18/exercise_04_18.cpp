#include <iostream>

int
main()
{
    int quantity, n = 1;

    std::cout << "Enter the number you want to print: ";
    std::cin  >> quantity;

    std::cout << "N\t10*N\t100*N\t1000*N\n" << std::endl;

    while (quantity >= n) {
        std::cout << n << "\t" << (10 * n) << "\t" << (100 * n) << "\t" << (1000 * n) << "\n";
        n++;
    }

    return 0;
}
