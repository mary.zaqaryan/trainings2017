#include <iostream>

int
main()
{
    std::cout << "A - " << static_cast<int>('A') << "\n";
    std::cout << "B - " << static_cast<int>('B') << "\n";
    std::cout << "C - " << static_cast<int>('C') << "\n";
    std::cout << "a - " << static_cast<int>('a') << "\n";
    std::cout << "b - " << static_cast<int>('b') << "\n";
    std::cout << "c - " << static_cast<int>('c') << "\n";
    std::cout << "0 - " << static_cast<int>('0') << "\n";
    std::cout << "1 - " << static_cast<int>('1') << "\n";
    std::cout << "2 - " << static_cast<int>('2') << "\n";
    std::cout << "$ - " << static_cast<int>('$') << "\n";
    std::cout << "* - " << static_cast<int>('*') << "\n";
    std::cout << "+ - " << static_cast<int>('+') << "\n";
    std::cout << "/ - " << static_cast<int>('/') << "\n";
    std::cout << "space - " << static_cast<int>(' ') << "\n";
    return 0;  
}

