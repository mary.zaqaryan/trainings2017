#include <iostream>
#include <iomanip>

int
main()
{
    double radius;
    std::cout << "Enter the radius of a circle: ";
    std::cin  >> radius;
    if (radius <= 0) {
        std::cerr << "Error 1: The radius of a circle cannot be negative!" << std::endl;
        return 1;
    }

    double pi = 3.14159;
    std::cout << "\nDiameter -> "
        << 2 * radius << std::endl;
    std::cout << "Area -> "
        << 2 * radius * radius << std::endl;
    std::cout << "Circumference -> "
        << std::setprecision(5)
        << std::fixed
        << 2 * pi * radius << std::endl;

    return 0;
}

