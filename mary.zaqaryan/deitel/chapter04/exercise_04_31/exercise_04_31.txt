When programmer wrote "++(x + y)", he/she thought,
that first operation would be x + y , then incrementing it.
Correct version is "x + y + 1".
