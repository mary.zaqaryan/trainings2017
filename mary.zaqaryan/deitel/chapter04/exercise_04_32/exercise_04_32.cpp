#include <iostream>

int 
main()
{  
    double side1;
    double side2;
    double side3;

    std::cout << "Enter 3 nonzero numbers. ";
    std::cin >> side1 >> side2 >> side3;
     
    if (side1 <= 0) {
        std::cerr << "Error 1: Side can't be zero or nedative.";
        return 1;
    }
    if (side2 <= 0) {
        std::cerr << "Error 1: Side can't be zero or nedative.";
        return 1;
    }
    if (side3 <= 0) {
        std::cerr << "Error 1: Side can't be zero or nedative.";
        return 1;
    }

    if (side1 < side2 + side3) {
        if (side2 < side1 + side3) {
            if (side3 < side1 + side2) {
                std::cout << "The triangle can be formed." << std::endl;
                return 0;
            }
        }
    }

    std::cout << "The triangle can't be formed" << std::endl;
    
    return 0; 
}
