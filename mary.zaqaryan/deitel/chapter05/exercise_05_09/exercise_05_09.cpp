#include <iostream>

int 
main()
{
    int prod = 1;
    
    for (int i = 1; i <= 15; i += 2) {
        prod *= i;
    }

    std::cout << "Product of odd numbers between 1 and 15 is " << prod << std::endl;

    return 0;
}
