#include <iostream>

int
main()
{
    int number1;
    int number2;
    std::cout << "Enter number 1: ";
    std::cin >> number1;
    std::cout << "Enter number 2: ";
    std::cin >> number2;
    
    int sum = number1 + number2;
    std::cout << "sum: " << sum << std::endl;

    int product = number1 * number2;
    std::cout << "product: " << product << std::endl;

    int diff = number1 - number2;
    std::cout << "difference: " << diff << std::endl;
    
    if (0 == number2) {
        std::cout << "Error 1: Division by 0.";
        return 1;
    }
    int quotient = number1 / number2;
    std::cout << "quotient: " << quotient << std::endl;

    return 0;
}
