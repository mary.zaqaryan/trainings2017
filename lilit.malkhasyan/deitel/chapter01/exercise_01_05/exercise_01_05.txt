Machine independent languages are easy to use for a wide range of users. 
A person doesn't need a complete knowledge of the specifics of computers. 
The alphabet of the algorithmic language is much wider than the alphabet of the computer language, 
which significantly increases the visibility of the text of the program.
The set of operations doesn't depend on a set of machine operations, 
but is chosen from considerations of convenience in the formulation of algorithms for solving problems of a certain class.
The format of the offers is flexible enough and convenient for use.
And finily i is quicklier.
