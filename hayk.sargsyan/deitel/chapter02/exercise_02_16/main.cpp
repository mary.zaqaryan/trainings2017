#include <iostream>
  
int
main()
{
    int number1;
    int number2;
  
    std::cout << "Please enter first number: ";
    std::cin >> number1;
 
    std::cout << "Please enter second number(please don't enter zero): ";
    std::cin >> number2;
 
    if (0 == number2) {
        std::cout << "Zero division error. Can not divide number to 0.\n";
    } else {
        std::cout << number1 << " + " << number2 << " = " << number1 + number2 << "\n";
        std::cout << number1 << " - " << number2 << " = " << number1 - number2 << "\n";
        std::cout << number1 << " * " << number2 << " = " << number1 * number2 << "\n";
        std::cout << number1 << " / " << number2 << " = " << number1 / number2 << "\n";
    }

    return 0;
}
