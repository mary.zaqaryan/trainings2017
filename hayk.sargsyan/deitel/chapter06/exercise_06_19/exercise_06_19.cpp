#include <iostream>
#include <cmath>

double
hypotenuse(const double sideA, const double sideB)
{
    const double hypotenuse = std::sqrt(sideA * sideA + sideB * sideB);

    return hypotenuse;
}

int
main()
{
    std::cout << "Triangle: 1\thypotenuse: " << hypotenuse(3.0, 4.0) << std::endl;
    std::cout << "Triangle: 2\thypotenuse: " << hypotenuse(5.0, 12.0) << std::endl;
    std::cout << "Triangle: 3\thypotenuse: " << hypotenuse(8.0, 15.0) << std::endl;

    return 0;
}
