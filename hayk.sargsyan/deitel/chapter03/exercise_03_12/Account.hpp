class Account
{
public:
    Account(int balance);
    void setBalance(int balance);
    int getBalance();
    int credit(int amount);
    int debit(int amount);
private:
    int balance_;
};
