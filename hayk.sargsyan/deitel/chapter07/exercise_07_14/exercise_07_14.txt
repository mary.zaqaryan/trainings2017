A)

Word "hello" contains 6 characters h, e, l, l, 0 and the end of the string
character. It means we can't take the word "hello" from keyboard because 
we have only 5 characters. Right code will be like this -

char str[6];

std::cin str; /// Typing "hello"


B) 

int a[3] means that we have a massive with 3 integer elements. The element
of a are a[0], a[1], a[2]. We have not element called a[3]. That's why 
program will not work. Correct code will be like this -

int a[3] = {1, 2, 3}

std::cout << a[0] << " " << a[1] << " " << a[2] << std::endl;


C)

We are declaring a massive like "double f[3]", it means that we have a 
massive that has 3 elements. Right code will be like this -

double f[4] = {1.1, 10.01, 100.001, 1000.0001};


D)

We have just a syntax error. Right syntax is -

double d [2][10];
d[1][9] = 2.345;
