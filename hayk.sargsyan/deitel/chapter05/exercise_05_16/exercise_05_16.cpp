#include <iostream>

int
main()
{
    int amount = 100000;
    int rate = 5;
    
    for (int year = 1; year <= 10; ++year) {
        amount = (amount * (100 + rate)) / 100;
        
        std::cout << "year:\t" 
                  << year
                  << std::endl
                  << "amount:\t"
                  << amount / 100 << "." << amount % 100 
                  << std::endl;
    }

    return 0;
}
