#include <iostream>

int 
main()
{
    int product = 1;
    std::cout << "The odd integers." << std::endl;
    for (int x = 1; x <= 15; x += 2) {
        std::cout << x << std::endl;
        product *= x;
    }
    std::cout << "The product of the odd integers is " << product << std::endl;
    return 0;
}

