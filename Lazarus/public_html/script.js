$(document).ready(function() {
    $('.table_image').on('mouseover', function() {
        $(this).width("23%");
    }).on('mouseout', function() {
        $(this).width("20%");
    });
    
    var narekZ = "<div class=\"Narek_Z\">\
                    <h3>Narek Zohrabyan</h3>\
                    <img class=\"table_image\" src=\"photo1.jpg\" alt=\"Narek Zohrabyan\" />\
                    <ul>\
                        <li><i>Nationality:</i> Armenian</li>\
                        <li><i>Date of Birth:</i> 1995</li>\
                        <li><i>Place of Birth:</i> Yerevan</li>\
                        <li><i>University:</i> The State Engineering University of Armenia (SEUA)</li>\
                        <li><i>Experience:</i> (August, 2017 - Now) Betconstruct IT Support Specialist</li>\
                        <li><i>Email:</i> narek.zohrabyan95@gmail.com</li>\
                    </ul>\
                </div>";
    var laura = "<div class=\"Laura\">\
                    <h3>Laura Hakobyan</h3>\
                    <img class=\"table_image\" src=\"photo2.jpg\" alt=\"Laura Hakobyan\" />\
                    <ul>\
                        <li><i>Nationality:</i> Armenian</li>\
                        <li><i>School:</i> No. 68</li>\
                        <li><i>University:</i> The State Engineering University of Armenia (SEUA)</li>\
                        <li><i>Working:</i> CANDLE SRI</li>\
                        <li><i>Email:</i> laura.hakobyan@bpcarm.org</li>\
                    </ul>\
                </div>";
    var sergey = "<div class=\"Sergey\">\
                    <h3>Sergey Hakobyan</h3>\
                    <img class=\"table_image\" src=\"photo3.jpg\" alt=\"Sergey Hakobyan\" />\
                    <ul>\
                        <li><i>Nationality:</i> Armenian</li>\
                        <li><i>School:</i> No. 68</li>\
                        <li><i>University:</i> The State Engineering University of Armenia (SEUA)</li>\
                        <li><i>Working:</i> Synopsis Armenia</li>\
                        <li><i>Email:</i> sergey.hakobyan@bpcarm.org</li>\
                    </ul>\
                </div>";
    var narekD = "<div class=\"Narek_D\">\
                    <h3>Narek Danielyan</h3>\
                    <img class=\"table_image\" src=\"photo4.jpg\" alt=\"Narek Danielyan\" />\
                    <ul>\
                        <li><i>Nationality:</i> Armenian</li>\
                        <li><i>School:</i> No. 47</li>\
                        <li><i>University:</i> YSU</li>\
                        <li><i>Email:</i> Danielyann729@gmail.com</li>\
                    </ul>\
                </div>";
    var tigran = "<div class=\"Tigran\">\
                    <h3>Tigran Sargsyan</h3>\
                    <img class=\"table_image\" src=\"photo5.jpg\" alt=\"Tigran Sargsyan\" />\
                    <ul>\
                        <li><i>Nationality:</i> Armenian</li>\
                        <li><i>Date of Birth:</i> 1997</li>\
                        <li><i>Place of Birth:</i> Yerevan</li>\
                        <li><i>University:</i> Russian-Armenian University (2014-2018)</li>\
                        <li><i>Email:</i> tigran.sargsyan@bpcarm.org</li>\
                    </ul>\
                </div>";
    $('.randomizer').click(function() {
        $('.table1Row').empty();
        $('.table2Row').empty();
        var member;
        var array = [0, 1, 2, 3, 4];
        for (var i = array.length; i > 0; --i) {
            var random = array.splice(Math.floor(Math.random() * i), 1)[0];
            switch (random) {
            case 0: member = narekZ; break;
            case 1: member = laura; break;
            case 2: member = sergey; break;
            case 3: member = narekD; break;
            case 4: member = tigran; break;
            }
            if (2 != $('.table1Row').children().length) {
                $('.table1Row').append('<td>' + member + '</td>');
            } else {
                $('.table2Row').append('<td>' + member + '</td>');
            }
        }
    });

    jQuery.fn.rotate = function(degrees) {
        $(this).css({'transform' : 'rotate3d(1, -1, 1,'+ degrees +'deg)'});
        return $(this);
    };

    var clicks = 0;
    var timeId;
    $('.lazarus').click(function() {
        var rotation = 0;
        if (!clicks) {
            timeId = setInterval(function() {
                if (360 == rotation) {
                    rotation = 0;
                }
                rotation += 5;
                $('.lazarus').rotate(rotation);
            }, 100);
            clicks++;
        } else {
            clearInterval(timeId);
            $('.lazarus').rotate(0);
            clicks--;
        }
    });
});