#include <string>

class Employee
{
public:
    Employee(std::string firstName, std::string lastName, int monthlySalary);
    void setFirstName(std::string firstName);
    void setLastName(std::string lastName);
    void setSalary(int monthlySalary);

    std::string getFirstName();
    std::string getLastName();
    int getSalary();
private: 
    std::string firstName_;
    std::string lastName_;
    int salary_;
};

