///Mikayel Ghaziyan
///11/10/2017
///Ecerxise 2.17


#include <iostream> // Using for output

///Beginning of the main function
int
main()
{
     ///Dsiplaying the result
     std::cout << "1 2 3 4 \n"; ///One statement, one stream 
     std::cout << "1 " << "2 " << "3 " << "4 \n"; /// One statement, 4 streams
     std::cout << "1 ";///Four statements 
     std::cout << "2 ";
     std::cout << "3 ";
     std::cout << "4 \n";

     return 0; ///Successfull end of the project

     ///End of the main     
}


///End of the file
