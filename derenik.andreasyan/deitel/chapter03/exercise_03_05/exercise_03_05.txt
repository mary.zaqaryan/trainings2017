A function prototype is a declaration of a function that specifies its name, argument types, and the returned data type.

The function definition contains, besides the declaration, the body of the function, which is a sequence of statements and descriptions in curly brackets.

The difference between a prototype and a function definition is that the prototype does not have a function body and its header ends with a semicolon.
