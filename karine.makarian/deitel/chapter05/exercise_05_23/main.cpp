#include <iostream>
#include <cmath>

int 
main()
{
    for (int i = -4; i <= 4; ++i) {
        for (int j = -4; j <= 4; ++j) {
            std::cout << ((std::abs(i) + std::abs(j) <= 4) ? "*" : " ");     
        }
        
        std::cout << std::endl;
    }
    return 0;
} 

