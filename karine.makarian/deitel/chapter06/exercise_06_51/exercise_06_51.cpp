#include <iostream>
#include <cmath>

int mystery(int, int); 

int main()
{
    int x, y;
    std::cout << "Enter two integers: ";
    std::cin >> x >> y;
    std::cout << "The result is " << mystery(x,y) << std::endl;

    return 0; 
} 

int 
mystery(int a, int b)
{
   if (0 == b) {
        return 0;
    }
    if (b < 0) {
        b = -b;
        a = -a;
    } 
    return a + mystery(a, b - 1);
}


