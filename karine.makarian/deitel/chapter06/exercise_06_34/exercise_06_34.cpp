#include <iostream>
#include <cstdlib>
#include <ctime>

bool 
flip()
{
    return static_cast<bool>(std::rand() % 2);
}

int
main()
{
    int tailsCounter = 0;
    int eagleCounter = 0;
    std::srand(std::time(0));
       
    for (int counter = 1; counter <= 100; ++counter) {
        if (flip()) {
            ++tailsCounter;
        } else {
            ++eagleCounter;
        }
    }

    std::cout << "flipped tails " << tailsCounter << " times" << std::endl
        << "flipped eagle " << eagleCounter << " times" << std::endl;

    return 0;
}

