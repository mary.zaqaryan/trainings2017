#include <iostream>
#include <cstdlib>
#include <ctime>

#ifdef FACTORIAL_TEMPLATE
template <int N> struct Factorial    { enum { value = N * Factorial<N - 1>::value }; };
template <>      struct Factorial<0> { enum { value = 1 }; };

inline
int
factorial(const int &/*n*/)
{
    //return Factorial<n>::value;
    return Factorial<9>::value;
}
#endif

#ifdef FACTORIAL_FAST
inline
int
factorial(const int& n)
{
    static const int ARR_SIZE = 17;
    ///assert(n > 0 && n < ARR_SIZE);
    static int factorialArray[ARR_SIZE] = { 0, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600, 1932053504, 1278945280, 2004310016, 2004189184 };
    return factorialArray[n];
}
#endif

#ifdef FACTORIAL_DAVID
int
factorial(int& n)
{
    int result = n;
    while (n > 3) {
        --n;
        result *= n;
    }
    return result * 2;
}
#endif

#ifdef FACTORIAL_ITER
int
factorial(const int& n)
{
    int f = 1;
    for (int i = n; i >= 2; --i) {
        f *= i;
    }
    return f;
}
#endif

#ifdef FACTORIAL_REC
int
factorial(const int& n)
{
    if (n < 2) {
        return 1;
    }
    return n * factorial(n - 1);
}
#endif

int
main()
{
    std::srand(std::time(NULL));
    for (int i = 1; i < 2000000000; ++i) {
        int x = std::rand() % 18;
        factorial(x);
    }
    std::cout << std::endl;
    return 0;
}

