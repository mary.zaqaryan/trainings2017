l = [1, 4, 9, 16, 25, 36];
print "l =", l;
print "l[0] =", l[0];
print "l[5] =", l[5];
print "l[-3] =", l[-3];
print "l[2:4] =", l[2:4];
print "l[0:3] =", l[0:3];
print "l[:3] =", l[:3];
print "l[2:] =", l[2:];
print "l[:] =", l[:];
print;

print "2 * [1, 4.3] + [5, 'str'] =", 2 * [1, 4.3] + [5, 'str'];
print;
# = [1, 4.3, 1, 4.3, 5, 'str'];

l[2:5] = ['a', 'b'];
print "l[2:5] = ['a', 'b'];"
print "l =", l;
print;

l.append(49);
print "l.append(49)";
print "l =", l;
print;

l.insert(0, 0);
print "l.insert(0, 0);";
print "l =", l;
print;

print "len(l) =", len(l);
print;

print "List of Lists";
ll = ['a', [1, 4], 'b', [5, 'hello'], 'c', ['a', [1, 4, ['m']]], 'd'];
print "ll =", ll;

