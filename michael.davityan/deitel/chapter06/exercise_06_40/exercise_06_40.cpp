#include <iostream>
#include <unistd.h>
#include <cassert>
#include <cstdlib>

long double power(long double base, long int exponent);


int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program raises a number to the power.\n";
        std::cout << "Insert the number: ";
    }
    long double base;
    std::cin >> base;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert the power of number: ";
    }
    long int exponent;
    std::cin >> exponent;
    std::cout << base << " to the power of " << exponent << " will be equal to " 
              << power(base, exponent) << std::endl;

    return 0;
}


long double 
power(long double base, long int exponent)
{
    if (0 == base) {
        return 0;
    }
    if (exponent > 0) {
        return (1 == exponent ? base : base * power(base, exponent - 1));
    }
    if (exponent < 0) {
        return (-1 == exponent ? 1 / base : 1 / base * power(base, exponent + 1));
    } 
    return 1.0;
}
