#include <iostream>
#include <cmath>

int roundToInteger(const double number);
double roundToTenths(const double number); 
double roundToHundreths(const double number); 
double roundToThousandths(const double number);

int
main()
{
    double floatingNumberWithPrecisionToOne;
    std::cin >> floatingNumberWithPrecisionToOne;
    std::cout << "base: " << floatingNumberWithPrecisionToOne << "\t" << "round to integer: " 
              << roundToInteger(floatingNumberWithPrecisionToOne) << "\n";

    double floatingNumberWithPrecisionToTwo;
    std::cin >> floatingNumberWithPrecisionToTwo;
    std::cout << "base: " << floatingNumberWithPrecisionToTwo << "\t" << "round to tenths: "
              << roundToTenths(floatingNumberWithPrecisionToTwo) << "\n";

    double floatingNumberWithPrecisionToThree;
    std::cin >> floatingNumberWithPrecisionToThree;
    std::cout << "base: " << floatingNumberWithPrecisionToThree << "\t" << "round to hundredths: "
              << roundToHundreths(floatingNumberWithPrecisionToThree) << "\n";

    double floatingNumberWithPrecisionToFour;
    std::cin >> floatingNumberWithPrecisionToFour;
    std::cout << "base: " << floatingNumberWithPrecisionToFour << "\t" << "round to thousandths: "
              << roundToThousandths(floatingNumberWithPrecisionToFour) << std::endl;
    return 0;
}

int 
roundToInteger(const double number)
{
    return std::floor(number + .5);
}

double
roundToTenths(const double number)
{
    return std::floor(number * 10 + .5) / 10;
} 

double
roundToHundreths(const double number) 
{
    return std::floor(number * 100 + .5) / 100;
}

double
roundToThousandths(const double number) 
{
    return std::floor(number * 1000 + .5) / 1000;
}
