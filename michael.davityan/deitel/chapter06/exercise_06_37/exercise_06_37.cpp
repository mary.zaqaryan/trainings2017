#include <iostream>
#include <cstdlib>
#include <cassert>
#include <ctime>
#include <unistd.h>

void printMessage(const bool answerStatus);
void printFinalMessage(const bool answerStatus);
void printPassMessage();
void printNotPassMessage();
void printBadStatus();

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }    
    int number1 = 0;
    int number2 = 0;
    bool answerStatus = true;
    const int QUESTIONS_LIMIT = 10;
    int trueAnswers = 0;
    for (int question = 1; question <= QUESTIONS_LIMIT; ++question) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is  "; 
        }
        if (answerStatus) {
            number1 = 1 + std::rand() % 10;
            number2 = 1 + std::rand() % 10;
        }
        if (::isatty(STDIN_FILENO)) {
            std::cout << number1 << " * " << number2 << "?: "; 
        }
        int answer;
        std::cin >> answer;
        answerStatus = (number1 * number2 == answer);
        if (answerStatus) {
            ++trueAnswers;
        }
        /// --------------------------------------------------------------------------------------------------------------
        /// This if for nice final message. For example if the user final answer prints wrong,
        /// it woldnt be good if prints "No. Please try again", but the program is finished. That is
        /// why i put this if. If final answer is true it prints "Good!!!", the otherwise "No. your attempts is expired".
        /// To understand it, please use the program in interactive mode.
        if ((question == QUESTIONS_LIMIT)) {
            printFinalMessage(answerStatus);
            break;
        }
        /// end of if....
        /// --------------------------------------------------------------------------------------------------------------
        printMessage(answerStatus);
    }
    const int TRUE_ANSWERS_MINIMAL_LIMIT = 3;
    if (trueAnswers < TRUE_ANSWERS_MINIMAL_LIMIT) {
        printBadStatus();
    }
    return 0;
}

void 
printMessage(const bool answerStatus) 
{
    if (answerStatus) {
        printPassMessage();
    } else {
        printNotPassMessage();
    }
}

void 
printPassMessage()
{
    std::cout << "Very good!\n";
}

void 
printNotPassMessage()
{
    std::cout << "No. Please try again.: ";
}

void 
printFinalMessage(const bool answerStatus)
{
    if (answerStatus) {
        std::cout << "Good!!!.\n";
    } else {
        std::cout << "No. Your attempts is expired!!!.\n";
    }
}

void 
printBadStatus()
{
    std::cout << "Please ask your instructor for extra help" << std::endl;
}
