#include <iostream>
#include <algorithm>

int
main()
{
    const int SIDE_LENGTH_LIMIT = 500;
    for (int hypotenuse = 5; hypotenuse <= SIDE_LENGTH_LIMIT; ++hypotenuse) {
        const int hypotenuseSquare = hypotenuse * hypotenuse;
        for (int catheter1 = 3; catheter1 < hypotenuse - 1; ++catheter1) {
            const int catheter1Square = catheter1 * catheter1;
            const int catheterLimit = std::max(catheter1, hypotenuse - catheter1 + 1);
            for (int catheter2 = hypotenuse - 1; catheter2 > catheterLimit; --catheter2) {
                const int catheter2Square = catheter2 * catheter2;
                if (hypotenuseSquare == catheter1Square + catheter2Square) {
                    std::cout << hypotenuse << ", " << catheter1 << ", " << catheter2 << std::endl;
                }
            }
        }
    }  
    return 0;
}
