#include "Date.hpp"
#include <iostream>

Date::Date(int day, int month, int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

void 
Date::setDay(int day)
{
    day_ = day;
}

void 
Date::setMonth(int month)
{
    if (month < 1) {
        std::cout << "Info 1: wrong month value.";
        return;
    }
    if (month > 12) {
        std::cout << "Info 1: wrong month value.";
        return;
    }

    month_ = month; 
}

void 
Date::setYear(int year)
{
    year_ = year;
}

int 
Date::getDay()
{
    return day_;
}

int 
Date::getMonth()
{
    return month_;
}

int 
Date::getYear()
{
    return year_;
}

void 
Date::displayDate()
{
    std::cout << getDay() << "/" << getMonth() << "/" << getYear() << std::endl;
}

